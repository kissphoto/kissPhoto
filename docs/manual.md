![kissPhotoLogo](../resources/images/KissPhotoIconLarge.png)

# Kissphoto Manual

## Basic Idea

keep it simple:
- just files (no database)
- parse filenames for renumbering and mass renaming
- cursor through lines in edit mode like in plain text
  
###No Data Loss
- "saving" will just rename the files
- deletion just results in moving the file into the subdirectory "deleted"
- rotation of jpgs is lossless and keeps the file date

      
## Install kissPhoto

[Download the installer for the latest version here](https://1drv.ms/f/s!Atee54Fx0NEeqBWXfQEVVw9_hnae)

Start the installer
- kissPhoto.msi for Windows
- kissPhoto.deb for Debian based Linux (e.g. Ubuntu)

Start kissPhoto.
Open the menu Extra/language and select your preferred language.

## Use kissPhoto
  
### Getting Started
- open a file with menu file/open...
- or drag a file to the left section of the application (the file table)

kissPhoto will open the directory of the file and selects the file    

Use the main menu or context menu to start a function of kissPhoto.

Hereby you will also learn the shortcuts which make editing very effective!
Many of them are based on windows products (e.g. F5 starts presentation mode, F2 edit mode, ...)

### Editing
Double Click an entry in the file table or press F2 to start inplace editing.

If multiple files are selected and you start editing you enter the mass renaming dialog.
                                             
###Save your changes
Your files will not be changed unless you save your changes.

This will result just in renaming the files.

Note: Rotation of JPEG files will be lossless, but here the files need to be rewritten.

